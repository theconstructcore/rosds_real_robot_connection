**Quick start**

## Instalation


1. Clone this git into your Device: `git clone https://bitbucket.org/theconstructcore/rosds_real_robot_connection.git`
2. Install curl: sudo apt install curl
3. Execute the setup script: `cd rosds_real_robot_connection; sudo ./realrobot_setup.sh`
4. Restart the system: `sudo reboot`
5. Check that the sustem is working: `systemctl status rosds_connector.service`

You should get the STATUS: Active: active 

---

## Establish Real Robot Connection

1. Open your web browser the page: http://localhost:3000 or http://IP_ROBOT_DEVICE:3000
2. Type in the "Device Name" field the name that you want to give to your device.
3. In ROSDS, Open the **Real Robot Tab**, and Click on **Turn On**.
4. Copy the URL given in the page http://IP_ROBOT_DEVICE:3000 and paste to ROSDS field below the device name.
