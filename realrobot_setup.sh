#! /usr/bin/env bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

echo "Warning_ You should have installed ROS before executing this script... Be warned!"


echo "-- Installing requirements --"
# Install Husarnet
curl https://install.husarnet.com/install.sh | sudo bash
sudo husarnet-firewall.sh disable
# Install NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
# Command to load NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
# Install required version of NPM
nvm install v10.15.3
nvm use v10.15.3


echo "-- Installing local web application --"
# Create ROSDS Connector web server
export CURRENT_DIR=$(pwd)
mkdir -p /usr/share/rosds_connector
rm -rf /usr/share/rosds_connector/{,.}*
git clone https://bitbucket.org/theconstructcore/robotwebapp.git /usr/share/rosds_connector
cd /usr/share/rosds_connector
npm install
cd $CURRENT_DIR


echo "-- Configuring .bashrc --"
# Configure .bashrc
bash -c "echo \"source /usr/share/rosds_connector/source.sh\" >> ~/.bashrc"
. ~/.bashrc


echo "-- Configuring /etc/hosts for AutoMode --"
# Add at the end of your ETC HOSTS
sudo bash -c "echo \"::1 master  # Added by Theconstruct\" >> /etc/hosts"
sudo bash -c "echo \"::1 $HOSTNAME  # Added by Theconstruct\" >> /etc/hosts"

# Create service file
cat <<EOT > /etc/systemd/system/rosds_connector.service
[Unit]
Description=ROSDS Connector
After=network.target


[Service]
ExecStart=/usr/share/rosds_connector/start.sh ${SUDO_USER}
ExecStop=/usr/share/rosds_connector/stop.sh ${SUDO_USER}
Restart=always
RestartSec=10                       # Restart service after 10 seconds if node service crashes
StandardOutput=syslog               # Output to syslog
StandardError=syslog                # Output to syslog
SyslogIdentifier=nodejs-example


[Install]
WantedBy=multi-user.target

EOT

# Start service and enable it at start up
systemctl daemon-reload
systemctl stop rosds_connector.service
systemctl start rosds_connector.service
systemctl enable rosds_connector.service


